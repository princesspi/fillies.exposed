<?php
require_once('conf/db_connect.php');
$id = $_GET['id'];
if(!strlen($id)==5) { die("Error: No Site Found here"); }
$sql = "SELECT url FROM fillies WHERE code=:id";
$q = $conn->prepare($sql);
$q->execute([':id'=>$id]);
$q->setFetchMode(PDO::FETCH_ASSOC);
$r = $q->fetch();
header("Location: {$r['url']}");