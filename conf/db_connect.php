<?php
require_once('db.php');

try {
    $conn = new PDO("mysql:host=$server;dbname=$database", $username, $password);
    //echo "Connected to $dbname at $host successfully.";
} catch (PDOException $pe) {
    die("Could not connect to the database $dbname :" . $pe->getMessage());
}