<?php
require_once('conf/db_connect.php');
$url = $_POST['url'];
$permitted_chars = '23456789ABCDEFGHJKMNPQRSTUVWXYZ';
$code = substr(str_shuffle($permitted_chars), 0, 5);


if(filter_var($url, FILTER_VALIDATE_URL)) {
    $task = array(':url' => $url,
	':code' => $code,
	':created' => time());
} else {
    echo("ERROR invalid URL"); die();
}
$sql = "INSERT INTO fillies (url,code,created) VALUES (:url, :code, :created);";
$q = $conn->prepare($sql);
$q->execute($task);

echo "https://fillies.exposed/$code";